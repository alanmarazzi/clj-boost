(ns clj-boost.core
  "The main clj-boost namespace.
  By requiring this namespace you get all functions for serializing,
  training and predicting XGBoost models."
  (:import (ml.dmlc.xgboost4j.java XGBoost
                                   Booster
                                   DMatrix)
           (clojure.lang PersistentArrayMap
                         PersistentVector
                         LazySeq
                         APersistentVector$SubVector)))

(defn- coll->array
  "Helper function to convert a nested vector to a flat array."
  [coll]
  (float-array (flatten coll)))

(defmulti dmatrix
          "Serializes given data to **DMatrix**.

          It is required by the XGBoost API to serialize data structures to use
          the library (https://xgboost.readthedocs.io/en/latest/jvm/java_intro.html).

          `dmatrix` tries to make the process as painless as possible internally
          dealing with types and variable arguments.

          It is possible to pass a map with `:x` and optionally `:y` keys,
          their values must be either a sequence of sequences or a vector of vectors
          for `:x` and a flat vector or sequence for `:y`.

          The input can also be a vector of vectors/sequence of sequences for *x*
          and optionally a flat vector/sequence for *y*.

          If given a string as input `dmatrix` loads a **DMatrix** from the
          given path in string form.

          When given a **Dmatrix** instance the result is the given **DMatrix**
          itself.

          *y* is required only for training, not for prediction.

          Examples:

          ```
          (def map-with-y {:x [[1 0] [0 1]] :y [1 0]})
          (dmatrix m)

          (def map-without-y {:x [[1 0] [0 1]]})
          (dmatrix m)

          (def vec-x [[1 0] [0 1]])
          (def vec-y [1 0])
          (dmatrix vec-x vec-y)
          (dmatrix vec-x)

          (def seq-x '((1 0) (0 1)))
          (def seq-y '(1 0))
          (dmatrix seq-x seq-y)
          (dmatrix seq-x)

          (dmatrix \"path/to/stored/dmatrix\")
          ```"
          (fn [d & _] (type d)))

(defmethod dmatrix PersistentArrayMap
  [{:keys [x y]}]
  (if y
    (doto
      (DMatrix.
        (coll->array x)
        (count y)
        (count (first x)))
      (.setLabel (coll->array y)))
    (DMatrix.
      (coll->array x)
      (count x)
      (count (first x)))))

(defmethod dmatrix PersistentVector
  ([x]
    (DMatrix.
      (coll->array x)
      (count x)
      (count (first x))))
  ([x y]
    (doto
      (DMatrix.
        (coll->array x)
        (count y)
        (count (first x)))
      (.setLabel (coll->array y)))))

(defmethod dmatrix APersistentVector$SubVector
  ([x]
    (DMatrix.
      (coll->array x)
      (count x)
      (count (first x))))
  ([x y]
    (doto
      (DMatrix.
        (coll->array x)
        (count y)
        (count (first x)))
      (.setLabel (coll->array y)))))

(defmethod dmatrix LazySeq
  ([x]
    (DMatrix.
      (coll->array x)
      (count x)
      (count (first x))))
  ([x y]
    (doto
      (DMatrix.
        (coll->array x)
        (count y)
        (count (first x)))
      (.setLabel (coll->array y)))))

(defmethod dmatrix String
  [path]
  (DMatrix. path))

(defmethod dmatrix DMatrix
  [dmat]
  dmat)

(defn nrow
  "Returns the number of rows in a **DMatrix**.

  Example:

  ```
  (nrow (dmatrix train))
  ;; 50
  ```"
  [dmatrix]
  (.rowNum dmatrix))

(defn- keywords->str
  "Helper to convert keywords in a map to strings.

  Example:

  ```(keywords->str {:key 1})
  ;; {\"key\" 1}```"
  [m]
  (into {}
        (for [[k v] m]
          [(str (name k)) v])))

(defn fit
  "Train an **XGBoost** model on the given data.

  The training set must be a **DMatrix** instance (see
  [[dmatrix]]) while `config` is a regular map.

  It returns a trained model (a **Booster** instance) that can
  be used for prediction or as a base margin for further training.

  `config` fields:

  - `:params` -> training parameters (see https://xgboost.readthedocs.io/en/latest/parameter.html)
  - `:rounds` -> number of boosting iterations
  - `:watches` -> a map of data to be evaluated during training. Usually either
    `{:train train-set}` to evaluate only on the training set or
    `{:train train-set :valid validation-set}`
  - `:early-stopping` -> training stops after this number of rounds of consecutive
    increases in any evaluation metric
  - `:booster` -> Optionally set an existing model to use as base margin

  Example:

  ```
  (fit (dmatrix x y)
    {:params {:eta 1.0}
     :rounds 2
     :watches {:train (dmatrix x y)}
     :early-stopping 10})
  ```"
  [dmatrix {:keys [params rounds
                   watches
                   early-stopping
                   booster] :as config}]
  (let [pars    (keywords->str params)
        watch   (keywords->str watches)
        metrics (make-array Float/TYPE
                            (count watch)
                            rounds)]
    (XGBoost/train dmatrix pars
                   rounds watch
                   metrics
                   nil nil
                   early-stopping
                   booster)))

(defn cross-validation
  "Perform cross-validation on the training set.

  The training set must be a **DMatrix** instance (see
  [[dmatrix]]) while `config` is a regular map.

  It returns a sequence of maps containing train and test
  error for every round as defined in the `config` map.

  `config` fields:

  - `:params`  -> training parameters (see https://xgboost.readthedocs.io/en/latest/parameter.html)
  - `:rounds`  -> number of boosting iterations
  - `:nfold`   -> number of folds for cross-validation
  - `:metrics` -> metrics to evaluate goodness of fit, must be a vector

  Example:

  ```
  (cross-validation (dmatrix x y)
                    {:params {:eta 1.0}
                     :rounds 5
                     :nfold 3})
  ```"
  [dmatrix {:keys [params rounds nfold metrics] :as config}]
  (let [pars (keywords->str params)
        met  (when-not (nil? metrics) (into-array String metrics))
        res  (XGBoost/crossValidation dmatrix pars
                                      rounds nfold
                                      met
                                      nil nil)]
    (for [s (vec res)
          :let [kv (re-seq #"(\S+):([0-9\.\,]+)" s)]]
      (reduce (fn [m [_ k v]]
                (assoc m (keyword k) (Double. (clojure.string/replace v "," "."))))
              {} kv))))

(defn predict
  "Use a trained model to make predictions.

  Given a **Booster** model and a **DMatrix** dataset uses
  the former to make predictions on the latter.

  Example:

  ```
  (predict (fit (dmatrix train) config)
           test-dmatrix)
  ```"
  [model dmatrix]
  (flatten (map seq (seq (.predict model dmatrix)))))

(defn load-model
  "Loads a saved **XGBoost** model.

  Given a path to a saved **XGBoost** model loads it and makes it usable.
  Returns a **Booster** instance.

  Example:

  ```
  (load-model \"path/to/model\")
  ```"
  [path]
  (XGBoost/loadModel path))

(defmulti persist
          "Save datasets and models in a format suited for **XGBoost**.

          Save either a **DMatrix** or a **Booster** instance to retrieve it for later use.

          Example:

          ```
          (persist (dmatrix dataset) \"path/to/dataset\")

          (persist (fit (dmatrix dataset) config) \"path/to/model\")
          ```"
          (fn [d other] (type d)))

(defmethod persist DMatrix [dmatrix path]
  (.saveBinary dmatrix path))

(defmethod persist Booster [model path]
  (.saveModel model path))

(defn pipe
  "Train-test-persist pipeline.

  This tries to reproduce a typical workflow: train a model on training data,
  test the model on the test dataset and optionally save the trained model.

  Example:

  ```
  (pipe (dmatrix train) (dmatrix test) config \"path/to/save/model\")
  ```"
  [train-dmatrix
   test-dmatrix
   config
   & [path]]
  (let [model (fit train-dmatrix config)
        pred  (predict model test-dmatrix)]
    (when path
      (println path)
      (persist model path))
    pred))

(defproject datascience/jcpsantiago.clj-boost "1.2.0"
  :description "A Clojure wrapper for XGBoost. Forked from alanmarazzi/clj-boost"
  :url "https://gitlab.com/jcpsantiago/clj-boost"
  :license {:name "Apache 2.0"
            :url  "https://www.apache.org/licenses/LICENSE-2.0"}
  :dependencies [[ml.dmlc/xgboost-jvm_2.12 "1.3.1" :extension "pom"]
                 [ml.dmlc/xgboost4j_2.12 "1.3.1" :extension "pom"]
                 [ml.dmlc/xgboost4j-example_2.12 "1.3.1" :extension "pom"]]
  :profiles {:dev {:dependencies [[org.clojure/clojure "1.10.1"]
                                  [org.clojure/data.csv "0.1.4"]
                                  [org.clojure/tools.deps.alpha "0.5.452"]
                                  [org.slf4j/slf4j-nop "1.7.25"]]
                   :plugins      [[lein-cloverage "1.0.13"]]}}
  :scm {:name "git"
        :url  "https://gitlab.com/alanmarazzi/clj-boost"})

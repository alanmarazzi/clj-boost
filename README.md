# clj-boost :sparkler:

[![Clojars Project](https://img.shields.io/clojars/v/clj-boost.svg?style=for-the-badge)](https://clojars.org/clj-boost)
[![Hex.pm](https://img.shields.io/hexpm/l/plug.svg?style=for-the-badge)](https://gitlab.com/alanmarazzi/clj-boost/blob/master/LICENSE)
[![cljdoc badge](https://cljdoc.org/badge/clj-boost/clj-boost)](https://cljdoc.org/d/clj-boost/clj-boost/CURRENT)


A Clojure wrapper for [XGBoost4J](https://github.com/dmlc/xgboost/tree/master/jvm-packages): train, store and predict using the full power of **XGBoost** directly from your **REPL**.

# Nota Bene

**clj-boost** is at this point considered a *done* library and there won't be any new functionality  or API changes. This is because of current new developments in the Clojure community around data science tooling: [**SciCloj**](https://scicloj.github.io/pages/about/).

**SciCloj** is partially answering to most of the [Rationale](#Rationale) below and I advise you to take a look at [this post](https://scicloj.github.io/posts/2019-05-23-clojure-scientists/) for better solutions.

By the way, **clj-boost** just works and if you like it better you can use it without any problem. The repo won't be closed/archived, debugging will go on, but if you'd like to add or update stuff (for instance XGBoost version) either do it locally or send a pull request.

## Rationale

Clojure is a great language for doing many things, but there's a field where it could shine and it doesn't: **data science** & **machine learning**. The main reason is the lack of domain libraries that would help practitioners to use off-the-shelf algorithms and solutions to do their work.

Python didn't become the leader in the field because it's inherently better or more performant, but because of [scikit-learn](http://scikit-learn.org/stable/), [pandas](https://pandas.pydata.org) and so on. While as Clojurists we don't really need [pandas](https://pandas.pydata.org) (dataframes) or similar stuff (everything is just a **map**, or if you care more about memory and performance a **record**) we don't have something like [scikit-learn](http://scikit-learn.org/stable/) that makes really easy to train many kind of machine learning models and somewhat easier to deploy them.

**clj-boost** clearly isn't a shot at [scikit-learn](http://scikit-learn.org/stable/) - something like that would require years of development - but it's a way to give people a better way to test and deploy their models. Clojure is robust, reliable and fast enough for most of the possible uses out there.

## Installation

Add to your leiningen `project.clj`:

```clojure
[clj-boost "1.0.0"]
```

For `tools.deps`:

```clojure
clj-boost {:mvn/version "1.0.0"}
```

## Usage

Start by requiring `clj-boost.core` in your namespace

```clojure
(ns tutorial
  (:require [clj-boost.core :refer :all]))
```

**XGBoost** forces us to use its data structures in exchange for speed and performance. So the first thing to do is to transform your data to a **DMatrix**. You can pass to `dmatrix` various data structures:

### Dmatrix

#### Map

It is possible to pass a map with `:x` and optionally `:y` keys, their values must be either a sequence of sequences or a vector of vectors for `:x` and a flat vector or sequence for `:y`. From now on everytime I use `x` and `y` I mean: `x` -> training data, `y` -> the objective to learn (required for training the model, optional for prediction)

```clojure
(dmatrix {:x [[0 1 0]
              [1 1 0]]
          :y [1 0]})

(dmatrix {:x [[0 1 0]
              [1 1 0]]})
```

#### Vector

The input can also be a vector of vectors/sequence of sequences for `x` and optionally a flat vector/sequence for `y`.

```clojure
(dmatrix [[[0 1 0]
           [1 1 0]]
          [1 0]])
                
(dmatrix [[0 1 0]
          [1 1 0]])
```

#### String

When given a string `dmatrix` tries to load a stored `dmatrix` on disk from the given path.

```clojure
(dmatrix "data/train-set.dmatrix")
```

There's not much we can do with a **DMatrix**, for instance once it is created it is impossible to go back to a regular data structure. At the moment the only possible operation is to get the number of rows from it:

```clojure
(nrow (dmatrix data))
;; 50
```

#### DMatrix

If given a **DMatrix** instance the result is the **DMatrix** itself:

```clojure
(let [dm (dmatrix data)]
  (dmatrix dm))
```

The result is `dm` itself.

###  Fitting

Now fitting a model is just a matter of calling `fit` on the **DMatrix** and as second argument a *config* map with parameters for the model. Parameters are the same for every **XGBoost** declination, so the advice is to use [this page](https://xgboost.readthedocs.io/en/latest/parameter.html) as a reference.

```clojure
(fit (dmatrix data)
     {:params {:eta 0.1
               :objective "binary:logistic"}
      :rounds 2
      :watches {:train (dmatrix data)
                :valid (dmatrix valid)}
      :early-stopping 10})
```

`fit` returns an **XGBoost** model instance, or a **Booster** for friends, that can be stored, used for prediction or as a baseline for further training. For the latter option just pass `:booster` to the parameters map with an already trained **Booster** instance.

```clojure
(fit (dmatrix data)
     {:params {:eta 0.1
               :objective "binary:logistic"}
      :rounds 2
      :watches {:train (dmatrix data)
                :valid (dmatrix valid)}
      :early-stopping 10
      :booster my-booster})
```

###  Cross-validation

`cross-validation` is basically the same, only you don't get a **Booster** in return, but the cross-validation results:

```clojure
(cross-validation (dmatrix data)
                  {:params {:eta 0.1
                            :objective "binary:logistic"}
                   :rounds 2
                   :nfold 3})
```

### Prediction

To get predictions there's the `predict` function that takes a model (a **Booster** instance) and data to predict.

```clojure
(-> (fit (dmatrix data)
     {:params {:eta 0.1
               :objective "binary:logistic"}
      :rounds 2
      :watches {:train (dmatrix data)
                :valid (dmatrix valid)}
      :early-stopping 10})
    (predict (dmatrix test-data))
```

### Persistence

Let's say that you're working either with large data or you're building an automated pipeline. Of course you would want to `persist` your models and your data for later use or as intermediate results. Finally, you will be able to `predict` new data by using `load-model` and getting ready for the data to come in:

```clojure
(persist (dmatrix data) "path/to/my-data")

(persist (dmatrix new-data) "path/to/my-new-data")

(-> (dmatrix "path/to/my-data")
    (fit 
     {:params {:eta 0.1
               :objective "binary:logistic"}
      :rounds 2
      :watches {:train (dmatrix data)
                :valid (dmatrix valid)}
      :early-stopping 10})
    (persist "path/to/my-model"))
    
(-> (load-model "path/to/my-model")
    (predict (dmatrix "path/to/my-new-data")))
```

### Pipe

Since this is a common pattern you might want to take a look at the `pipe` function: it takes *train-dmatrix*, *test-dmatrix*, *config* and optionally a *path*. `pipe` will train a model by using *config* as parameters, make predictions on given test data and if a *path* is given it will store the model at *path*.

```clojure
(pipe (dmatrix data)
      (dmatrix new-data)
      {:params {:eta 0.1
               :objective "binary:logistic"}
      :rounds 2
      :watches {:train (dmatrix data)
                :valid (dmatrix valid)}
      :early-stopping 10}
      "path/to/my-model")
```

## Tutorials

You can find a [**demo**](https://gitlab.com/alanmarazzi/clj-boost/tree/master/demo) folder in this repo where there are self-contained scripts and examples. In the [**doc**](https://gitlab.com/alanmarazzi/clj-boost/tree/master/doc) folder there are guides and tutorials that you can find hosted on [clj-doc](https://cljdoc.org/d/clj-boost/clj-boost/CURRENT) as well as longer posts on my [personal blog](https://www.rdisorder.eu/tag/clj-boost)

## ~~To do~~

- [x] Make some tutorials and posts about `clj-boost` usage
- [ ] ~~Add CI/CD to the repo~~
- [ ] ~~Reach feature parity with XGBoost4J~~
- [ ] ~~Add a method to generate *config* programmatically (atom?)~~
- [ ] ~~Add a way to perform grid search over parameters~~
- [ ] ~~Find a way to make prediction more tunable~~
- [ ] ~~Some facilities (like accuracy, confusion matrix, etc)???~~

## License

© Alan Marazzi, 2019. Licensed under an [Apache-2](https://gitlab.com/alanmarazzi/clj-boost/blob/master/LICENSE) license.

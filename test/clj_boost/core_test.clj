(ns clj-boost.core-test
  (:require [clojure.test :refer :all]
            [clojure.java.io :as io]
            [clojure.data.csv :as csv]
            [clj-boost.core :refer :all])
  (:import (ml.dmlc.xgboost4j.java DMatrix
                                   Booster)))

(def iris-path "resources/iris.csv")

(defn- generate-iris
  []
  (with-open [reader (io/reader iris-path)]
    (doall
      (->> (csv/read-csv reader)
           (drop 1)
           (mapv #(split-at 4 %))
           (drop-last 50)))))

(defn- make-data
  [iris-path]
  (let [data (generate-iris)
        x    (->> (map first data)
                  (map #(map read-string %))
                  (mapv vec))
        y    (->> (map last data)
                  (mapv (fn [class]
                          (if (= (first class) "setosa")
                            0
                            1))))]
    [x y]))

(defn make-train-vec
  [iris-path]
  (let [[x y] (make-data iris-path)]
    [(subvec (vec x) 25 75)
     (subvec (vec y) 25 75)]))

(defn make-test-vec
  [iris-path]
  (let [[x y] (make-data iris-path)
        [f-x f-y] (map #(take 25 %) [x y])
        [l-x l-y] (map #(drop 75 %) [x y])]
    [(vec (concat f-x l-x))
     (vec (concat f-y l-y))]))

(defn make-train-map
  [iris-path]
  (let [[x y] (make-train-vec iris-path)]
    {:x (vec x) :y (vec y)}))

(defn make-test-map
  [iris-path]
  (let [[x y] (make-test-vec iris-path)]
    {:x (vec x) :y (vec y)}))

(def train-vec
  (make-train-vec iris-path))

(def test-vec
  (make-test-vec iris-path))

(def train-map
  (make-train-map iris-path))

(def test-map
  (make-test-map iris-path))

(deftest dmatrix-test-instance
  (are [d] (instance? DMatrix d)
           (dmatrix train-map)
           (dmatrix train-vec)
           (dmatrix (:x train-map))
           (dmatrix (first train-vec))))

(def filename (atom []))

(defn manage-resources
  [test-fn]
  (.saveBinary (dmatrix train-map) "resources/dm")
  (swap! filename conj "resources/dm")
  (.saveModel (fit (dmatrix train-map)
                   {:params         {:eta 0.1}
                    :rounds         1
                    :watches        {:train (dmatrix [[0 1]
                                                      [1 0]]
                                                     [1 0])}
                    :early-stopping 10}) "resources/model")
  (swap! filename conj "resources/model")
  (test-fn)
  (dorun (map #(io/delete-file (io/file %)) @filename))
  (reset! filename []))

(use-fixtures :once manage-resources)

(deftest dmatrix-from-file
  (instance? DMatrix (dmatrix "resources/dm")))

(deftest dmatrix-test-label
  (are [label dm]
    (= label (seq (.getLabel dm)))
    [1.0 0.0] (dmatrix [[0 1] [1 0]] [1 0])
    nil (dmatrix (:x train-map))
    (map float (second train-vec)) (dmatrix (first train-vec)
                                            (second train-vec))))

(deftest nrow-test
  (are [n d] (= n (nrow d))
             0 (DMatrix. (float-array 0) 0 0)
             1 (dmatrix [[]])
             50 (dmatrix train-map)))

(deftest fit-test-instance
  (are [d c] (instance? Booster (fit d c))
             (dmatrix [[0 1] [1 0]] [1 0])
             {:params         {:eta 0.1}
              :rounds         1
              :watches        {:train (dmatrix [[0 1]
                                                [1 0]]
                                               [1 0])}
              :early-stopping 10}

             (dmatrix train-map)
             {:params         {:eta 0.1}
              :rounds         1
              :watches        {:train (dmatrix train-map)}
              :early-stopping 10}

             (dmatrix (first train-vec)
                      (second train-vec))
             {:params         {:eta 0.1}
              :rounds         1
              :watches        {:train (dmatrix (first train-vec)
                                               (second train-vec))}
              :early-stopping 10}

             (dmatrix test-map)
             {:params         {:eta 0.1}
              :rounds         1
              :watches        {:train (dmatrix test-map)}
              :early-stopping 10
              :booster        (fit (dmatrix train-map)
                                   {:params         {:eta 0.1}
                                    :rounds         1
                                    :watches        {:train
                                                     (dmatrix train-map)}
                                    :early-stopping 10})}))

(deftest cross-validation-test-result
  (is (= 1 (count (cross-validation
                    (dmatrix train-map)
                    {:params {}
                     :rounds 1
                     :nfold  2}))))
  (is (= 2 (count (cross-validation
                    (dmatrix train-map)
                    {:params {}
                     :rounds 2
                     :nfold  2}))))
  (is (do (java.util.Locale/setDefault java.util.Locale/ITALY)
          (= 2 (count (cross-validation
                        (dmatrix train-map)
                        {:params {}
                         :rounds 2
                         :nfold  2})))))
  (is (= 2 (count (first (cross-validation
                           (dmatrix train-map)
                           {:params  {}
                            :rounds  1
                            :nfold   2
                            :metrics ["error"]})))))
  (is (= 4 (count (first (cross-validation
                           (dmatrix train-map)
                           {:params  {}
                            :rounds  1
                            :nfold   2
                            :metrics ["error" "auc"]}))))))

(deftest persist-dmatrix-test
  (are [d fname] (do
                   (swap! filename conj fname)
                   (persist (dmatrix d) (last @filename))
                   (.exists (io/file (last @filename))))
    train-map "resources/dmat"
    (first train-vec) "resources/dmat2"
    [[]] "resources/dmat3"))

(deftest persist-booster-test
  (are [d fname] (do
                   (swap! filename conj fname)
                   (persist (fit (dmatrix d)
                                 {:params         {}
                                  :rounds         1
                                  :watches        {:train (dmatrix d)}
                                  :early-stopping 10})
                            (last @filename))
                   (.exists (io/file (last @filename))))

                 train-map "resources/booster"
                 test-map "resources/booster2"))

(deftest load-model-test
  (is (instance? Booster (load-model "resources/model"))))

(deftest predict-test
  (let [train-data (dmatrix train-map)
        test-data  (dmatrix test-map)
        f          (fn [d] (fit d {:params         {}
                                   :rounds         1
                                   :watches        {:train d}
                                   :early-stopping 10}))]
    (is (seq? (predict (f train-data) test-data)))
    (is (= (count (:y test-map))
           (count (predict (f train-data) test-data))))
    (is (seq? (predict (load-model "resources/model") test-data)))))

(deftest pipe-test-simple
  (is (seq? (pipe (dmatrix train-map)
                  (dmatrix test-map)
                  {:params         {}
                   :rounds         1
                   :watches        {:train (dmatrix train-map)}
                   :early-stopping 10})))
  (is (= (count (:y test-map))
         (count (pipe (dmatrix train-map)
                      (dmatrix test-map)
                      {:params         {}
                       :rounds         1
                       :watches        {:train (dmatrix train-map)}
                       :early-stopping 10}))))
  (is (seq? (do (swap! filename conj "resources/pipe")
                (pipe (dmatrix train-map)
                      (dmatrix test-map)
                      {:params         {}
                       :rounds         1
                       :watches        {:train (dmatrix train-map)}
                       :early-stopping 10}
                      "resources/pipe"))))
  (is (= (count (:y test-map))
         (count (pipe (dmatrix train-map)
                      (dmatrix test-map)
                      {:params         {}
                       :rounds         1
                       :watches        {:train (dmatrix train-map)}
                       :early-stopping 10}
                      "resources/pipe")))))

(deftest pipe-test-file
  (are [fname f-test] (do
                        (swap! filename conj fname)
                        (pipe (dmatrix train-map)
                              (dmatrix test-map)
                              {:params         {}
                               :rounds         1
                               :watches        {:train (dmatrix train-map)}
                               :early-stopping 10}
                              (last @filename))
                        (f-test (last @filename)))
                      "resources/pipe-model" #(.exists (io/file %))
                      "resources/pipe-model2" (fn [x]
                                                (instance?
                                                  Booster
                                                  (load-model x)))
                      "resources/pipe-model3" (fn [x]
                                                (= (count (:y test-map))
                                                   (count (predict
                                                            (load-model x)
                                                            (dmatrix test-map)))))))
